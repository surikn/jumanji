import argparse
import yaml

from pytorch_lightning import Trainer

from jumanji.config import PipelineConfig, TrainerConfig, ClassBox

from jumanji.callbacks import ModelCheckpoint

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config_path", type=str, help="Path to .yml file with configuration parameters ."
    )
    config_path = parser.parse_args().config_path

    config_yaml = yaml.load(open(config_path, "r"), Loader=yaml.FullLoader)
    pipeline_params = PipelineConfig(**config_yaml["pipeline"])
    trainer_params = TrainerConfig(**config_yaml["trainer"])

    logger_params = trainer_params.logger
    trainer_params.logger = ClassBox.loggers[logger_params.name](**logger_params.params)

    callbacks_params_list = trainer_params.callbacks
    if callbacks_params_list:
        trainer_params.callbacks = [ClassBox.callbacks[callbacks_params.name](**callbacks_params.params)
                                    for callbacks_params in callbacks_params_list]

        trainer_params.callbacks = [callback for callback in trainer_params.callbacks if callback]

    trainer_params.checkpoint_callback = ModelCheckpoint(**trainer_params.checkpoint_callback)

    pipeline = ClassBox.pipelines[pipeline_params.name](
        models_params=pipeline_params.models,
        criterions_params=pipeline_params.criterions,
        optimizers_params=pipeline_params.optimizers,
        schedulers_params=pipeline_params.schedulers,
        metrics_params=pipeline_params.metrics,
        data_params=pipeline_params.data,
    )

    trainer = Trainer(**trainer_params.dict())
    trainer.fit(pipeline)
    trainer.test(pipeline)