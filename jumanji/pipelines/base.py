from collections import namedtuple

import torch
from torch.nn import Module
from torch.utils.data import WeightedRandomSampler
from pytorch_lightning import LightningModule
from albumentations.core.composition import BaseCompose


class Pipeline(LightningModule):
    def __init__(
            self,
            models_params,
            criterions_params,
            optimizers_params,
            schedulers_params,
            metrics_params,
            data_params,
    ):
        super().__init__()
        self.data_params = data_params
        self.optimizers_params = optimizers_params
        self.schedulers_params = schedulers_params

        from jumanji.config import ClassBox

        models = {
            model_name: ClassBox.models[model.name](**model.params)
            for model_name, model in models_params.items()
        }

        self.models = Module()
        for model_name, model in models.items():
            self.models.__setattr__(model_name, model)

        # self.models = namedtuple("models", models.keys())(**models)

        criterions = {
            criterion_name: ClassBox.criterions[criterion.name](**criterion.params)
            for criterion_name, criterion in criterions_params.items()
        }

        self.criterions = namedtuple("criterions", criterions.keys())(**criterions)

        metrics = {
            metric_name: ClassBox.metrics[metric.name](**metric.params)
            for metric_name, metric in metrics_params.items()
        }

        self.metrics = namedtuple("metrics", metrics.keys())(**metrics)

    def configure_optimizers(self):
        from jumanji.config import ClassBox

        # TODO: add mapping to model
        optimizers = [
            ClassBox.optimizers[optimizer.name](self.models.model.parameters(), **optimizer.params)
            for optimizer in self.optimizers_params
        ]

        # TODO: add optimizer-scheduler mapping
        schedulers = [
            ClassBox.optimizers[scheduler.name](self.models.model.parameters(), **scheduler.params)
            for scheduler in self.schedulers_params
        ]

        return optimizers, schedulers

    def configure_data(self, params):
        from jumanji.config import ClassBox

        # TODO: Kostil
        params.dataset.params["transform"] = parse_transforms(params.dataset.params["transform"], ClassBox.transforms)
        augment = params.dataset.params.get("augment")
        if augment:
            params.dataset.params["augment"] = parse_transforms(augment, ClassBox.transforms)
        dataset = ClassBox.datasets[params.dataset.name](**params.dataset.params)
        # TODO: Add universal sampler
        sampler = params.dataloader.params.get("sampler")
        if sampler:
            params.dataloader.params["sampler"] = prepare_sampler(dataset)
        return ClassBox.dataloaders[params.dataloader.name](
            dataset, **params.dataloader.params
        )

    def train_dataloader(self):
        train_params = self.data_params.train
        return self.configure_data(train_params)

    def val_dataloader(self):
        valid_params = self.data_params.eval
        return self.configure_data(valid_params)

    def test_dataloader(self):
        test_params = self.data_params.test
        return self.configure_data(test_params)


def parse_transforms(tranforms_yaml, tranforms_dict):
    transform_name = tranforms_yaml["name"]
    transform_class = tranforms_dict[transform_name]
    if issubclass(transform_class, BaseCompose):
        params = tranforms_yaml["params"]
        transforms = params.pop("transforms")
        transforms = [parse_transforms(transform, tranforms_dict) for transform in transforms]
        return transform_class(transforms=transforms, **params)
    else:
        return transform_class(**tranforms_yaml.get("params", {}))


def prepare_sampler(dataset):
    class_count = dataset.csv.label.value_counts()
    class_count = [class_count[label] for label in range(len(class_count))]

    target_list = torch.tensor(dataset.csv.label)
    target_list = target_list[torch.randperm(len(target_list))]

    class_weights = 1 - torch.tensor(class_count, dtype=torch.float) / torch.tensor(sum(class_count), dtype=torch.float)

    class_weights_all = class_weights[target_list]

    weighted_sampler = WeightedRandomSampler(
        weights=class_weights_all,
        num_samples=len(class_weights_all),
        replacement=True
    )

    return weighted_sampler
