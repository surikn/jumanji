import random

import torch
import cv2
import numpy as np
from albumentations.core.transforms_interface import ImageOnlyTransform, DualTransform
from albumentations import functional as F


class ToChannels(ImageOnlyTransform):
    def apply(self, sample, **params):
        res = []
        for s in sample:
            res.append(s[0])
            res.append(s[1])
        return res


class ChannelsToTensor(ImageOnlyTransform):
    """Convert ndarrays in sample to Tensors."""

    def apply(self, sample, **params):
        return torch.Tensor(np.vstack([s[np.newaxis, :] for s in sample]))


class GrayScale(ImageOnlyTransform):
    def __init__(self, num_output_channels=1, always_apply=False, p=1):
        super().__init__(always_apply, p)
        self.num_output_channels = num_output_channels

    def apply(self, sample, **params):
        gray = cv2.cvtColor(sample, cv2.COLOR_RGB2GRAY)
        if self.num_output_channels == 1:
            return np.expand_dims(gray, -1)
        else:
            return cv2.merge([gray] * self.num_output_channels)


class ProportionalCenterCrop(DualTransform):
    def __init__(self, p_height_range, p_width_range, always_apply=False, p=1.0):
        super(ProportionalCenterCrop, self).__init__(always_apply, p)
        self.p_height_range = p_height_range
        self.p_width_range = p_width_range

    def apply(self, img, height=0, width=0, **params):
        return F.center_crop(img, height, width)

    def apply_to_bbox(self, bbox, height=0, width=0, **params):
        return F.bbox_random_crop(bbox, height, width, **params)

    def apply_to_keypoint(self, keypoint, height=0, width=0, **params):
        return F.keypoint_random_crop(keypoint, height, width, **params)

    def get_params_dependent_on_targets(self, params):
        image = params['image']
        h, w = image.shape[:2]
        crop_h = int(h * random.uniform(*self.p_height_range))
        crop_w = int(w * random.uniform(*self.p_width_range))

        return {'height': crop_h,
                'width': crop_w}

    def get_transform_init_args_names(self):
        return 'p_height_range', 'p_width_range'

    @property
    def targets_as_params(self):
        return ['image']
