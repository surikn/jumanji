from torch import nn


class Model8(nn.Module):
    def __init__(self, in_chans=8):
        super(Model8, self).__init__()

        features = [
            nn.BatchNorm3d(in_chans),
            nn.Conv3d(in_chans, 128, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool3d(2, stride=2),
            nn.Conv3d(128, 256, 3, padding=1),
            nn.ReLU(),
            nn.MaxPool3d(2, stride=2),
            nn.Conv3d(256, 512, 3, padding=1),
            nn.ReLU(),
            nn.AdaptiveAvgPool3d((4, 2, 2)),
        ]

        head = [
            nn.Linear(1024 * in_chans, 1024),
            nn.ReLU(),
            nn.Dropout3d(),
            nn.Linear(1024, 1),
            nn.Sigmoid(),
        ]

        self.features = nn.Sequential(*features)
        self.head = nn.Sequential(*head)

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.head(x)
        return x
