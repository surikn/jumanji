from pytorch_lightning.metrics import functional


class Metric:
    def __init__(self, **params):
        if params is None:
            params = {}
        self.params = params

    def __call__(self, *args, **kwargs):
        raise NotImplementedError


class LigthningMetric(Metric):
    def __init__(self, **params):
        assert "name" in params

        self.name = params.pop("name")
        self.lightning_metrics: dict = {k: v for k, v in functional.__dict__.items() if callable(v)}

        assert self.name in self.lightning_metrics
        super().__init__(**params.get("params", {}))

    def __call__(self, *args):
        return self.lightning_metrics[self.name](*args, **self.params)


class PPV(Metric):
    def __call__(self, y_scores, y_true):
        return y_true[y_scores.argsort(descending=True)][:y_true.sum().int()].float().mean()
