from torch import optim
from torch.optim import lr_scheduler
from torch.optim.lr_scheduler import _LRScheduler
from torch.utils.data.dataset import Dataset
from torch.utils.data.dataloader import DataLoader
from pytorch_lightning import loggers
from pytorch_lightning.loggers import LightningLoggerBase
from torch.nn.modules import loss
from torch.nn import Module

from jumanji import models
from jumanji.data import datasets
from jumanji import pipelines
from jumanji.pipelines.base import Pipeline
from jumanji import criterions
# from jumanji.metrics import LightningMetricFactory
from jumanji import callbacks
from jumanji.callbacks import Callback
from jumanji import metrics
from jumanji.metrics import Metric
from jumanji.data import preprocess
from albumentations.core.transforms_interface import BasicTransform
from albumentations.core.composition import BaseCompose


class ClassBox:
    models: dict = {
        k: v
        for k, v in models.__dict__.items()
        if isinstance(v, type) and issubclass(v, Module)
    }

    datasets: dict = {
        k: v
        for k, v in datasets.__dict__.items()
        if isinstance(v, type) and issubclass(v, Dataset)
    }

    pipelines: dict = {
        k: v
        for k, v in pipelines.__dict__.items()
        if isinstance(v, type) and issubclass(v, Pipeline)
    }

    optimizers: dict = {
        k: v
        for k, v in optim.__dict__.items()
        if isinstance(v, type) and issubclass(v, optim.Optimizer)
    }

    schedulers: dict = {
        k: v
        for k, v in lr_scheduler.__dict__.items()
        if isinstance(v, type) and issubclass(v, _LRScheduler)
    }

    dataloaders: dict = {"Default": DataLoader}

    criterions: dict = {
        k: v
        for k, v in criterions.__dict__.items()
        if isinstance(v, type) and issubclass(v, loss._Loss)
    }

    metrics: dict = {
        k: v
        for k, v in metrics.__dict__.items()
        if isinstance(v, type) and (issubclass(v, Metric))
    }

    loggers: dict = {
        k: v
        for k, v in loggers.__dict__.items()
        if isinstance(v, type) and issubclass(v, LightningLoggerBase)
    }

    transforms: dict = {
        k: v
        for k, v in preprocess.__dict__.items()
        if isinstance(v, type) and (issubclass(v, BasicTransform) or issubclass(v, BaseCompose))
    }

    callbacks: dict = {
        k: v
        for k, v in callbacks.__dict__.items()
        if isinstance(v, type) and issubclass(v, Callback)
    }
